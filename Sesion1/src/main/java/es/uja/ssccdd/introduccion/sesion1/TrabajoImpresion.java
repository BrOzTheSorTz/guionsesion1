package es.uja.ssccdd.introduccion.sesion1;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pedroj
 */
public class TrabajoImpresion {
    private final int idTrabajo;
    private final int tiempoTrabajo;
    private static int contId = 0;

    /**
    *Construir un trabajo con un id unico
    */
    public TrabajoImpresion( int tiempoTrabajo) {
        this.idTrabajo = contId++;
        this.tiempoTrabajo = tiempoTrabajo;
    }

    public int getIdTrabajo() {
        return idTrabajo;
    }

    public int getTiempoTrabajo() {
        return tiempoTrabajo;
    }

    @Override
    public String toString() {
        return "TrabajoImpresion{" + "idTrabajo=" + idTrabajo + ", tiempoTrabajo=" 
                + tiempoTrabajo + '}';
    }
}
